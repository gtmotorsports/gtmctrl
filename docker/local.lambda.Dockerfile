FROM golang:1 as gtm-build-lambda
COPY ./gtm/gtmctrl /apps/gtm/gtmctrl/
COPY ./projects/house-points-manager /apps/projects/house-points-manager/
COPY ./projects/house-points /apps/projects/house-points/
COPY ./projects/logging-go /apps/projects/logging-go/
COPY ./projects/tracing-go /apps/projects/tracing-go/
COPY ./projects/slack-app /apps/projects/slack-app/
WORKDIR /apps/gtm/gtmctrl
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg/mod \
    go mod tidy && \
    go build -o ./build/lambda/gtmctrl ./cmd/lambda

FROM public.ecr.aws/lambda/go:1
COPY --from=gtm-build-lambda /apps/gtm/gtmctrl/build/lambda/gtmctrl ${LAMBDA_TASK_ROOT}
CMD ["gtmctrl"]
