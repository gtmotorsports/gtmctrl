FROM golang:1 as gtm-build-lambda
WORKDIR /app
COPY . .
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg/mod \
    go mod tidy && \
    go build  -o ./build/lambda/gtmctrl ./cmd/lambda

FROM public.ecr.aws/lambda/go:1
COPY --from=gtm-build-lambda /app/build/lambda/gtmctrl ${LAMBDA_TASK_ROOT}
CMD ["gtmctrl"]
