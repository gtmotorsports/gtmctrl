package main

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/rs/zerolog/log"
	slackapp "gitlab.com/johnrichter/slack-app"
	traceext "gopkg.in/DataDog/dd-trace-go.v1/ddtrace/ext"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

const (
	slackFeaturePathParamKey = "slack-feature"
)

var (
	ErrorGWInternalServer = events.APIGatewayProxyResponse{StatusCode: http.StatusInternalServerError}
	ErrorGWBadRequest     = events.APIGatewayProxyResponse{StatusCode: http.StatusBadRequest}
)

type ApiGwProxy struct {
	config   *GtmCtrlConfig
	slackApp *slackapp.SlackApp
}

func NewApiGwProxy(rc *GtmCtrlConfig, slackApp *slackapp.SlackApp) *ApiGwProxy {
	return &ApiGwProxy{
		config:   rc,
		slackApp: slackApp,
	}
}

func (r *ApiGwProxy) HandleAPIGatewayRequest(
	agwRequest *events.APIGatewayProxyRequest,
) (*events.APIGatewayProxyResponse, error) {
	req, err := r.toNativeRequest(agwRequest)
	if err != nil {
		log.Error().Err(err).Msg("Unable to translate APIGatewayProxyRequest to native request")
		return &ErrorGWInternalServer, nil
	}
	slackFeature, ok := agwRequest.PathParameters[slackFeaturePathParamKey]
	if !ok {
		log.Warn().Msg("Request missing Slack Feature parameter")
		return &ErrorGWBadRequest, nil
	}
	span, _ := tracer.SpanFromContext(r.config.ctx)
	span.SetTag(traceext.ResourceName, "slack.callback")

	resp, err := r.slackApp.HandleSlackCallback(slackFeature, req)
	if err != nil {
		switch err := err.(type) {
		case *slackapp.SlackAppError:
			log.Warn().Err(err).Msg("Unable to process Slack callback")
			return &events.APIGatewayProxyResponse{StatusCode: err.StatusCode, Body: err.AsJson()}, nil
		}
		log.Error().Err(err).Msg("An unknown error occurred while processing the Slack callback")
		return &ErrorGWInternalServer, nil
	}
	agwResponse := events.APIGatewayProxyResponse{
		StatusCode: resp.StatusCode,
		Headers:    resp.Headers,
		Body:       resp.AsJson(),
	}
	return &agwResponse, nil
}

func (r *ApiGwProxy) toNativeRequest(gwr *events.APIGatewayProxyRequest) (*http.Request, error) {
	req, err := http.NewRequestWithContext(
		r.config.ctx,
		gwr.HTTPMethod,
		gwr.Path,
		strings.NewReader(gwr.Body),
	)
	if err != nil {
		return nil, err
	}
	// for k, v := range gwr.Headers {
	// 	req.Header.Add(k, v)
	// }
	for k, mv := range gwr.MultiValueHeaders {
		for _, v := range mv {
			req.Header.Add(k, v)
		}
	}
	queryParams := url.Values{}
	// for k, v := range gwr.QueryStringParameters {
	// 	queryParams.Add(k, v)
	// }
	for k, mv := range gwr.MultiValueQueryStringParameters {
		for _, v := range mv {
			queryParams.Add(k, v)
		}
	}
	req.URL.RawQuery = queryParams.Encode()
	return req, nil
}
