package main

var (
	ErrDecryptSecrets = HousePointsManagerError{Message: "Unable to decrypt configuration secrets"}
)

type HousePointsManagerError struct {
	Message string `json:"message"`
}

func (e *HousePointsManagerError) Error() string {
	return e.Message
}
