package main

import (
	"context"
	"net/http"
	"os"

	ddlambda "github.com/DataDog/datadog-lambda-go"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/rs/zerolog/log"
	hpmanager "gitlab.com/johnrichter/house-points-manager"
	hpeventbus "gitlab.com/johnrichter/house-points/event/bus"
	slackapp "gitlab.com/johnrichter/slack-app"
	"gitlab.com/johnrichter/tracing-go"
	traceext "gopkg.in/DataDog/dd-trace-go.v1/ddtrace/ext"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

func init() {
	// Trace all http calls
	http.DefaultClient = tracing.DefaultHTTPClient

	env := os.Getenv("DD_ENV")
	service := os.Getenv("DD_SERVICE")
	version := os.Getenv("DD_VERSION")
	log.Logger = log.With().Fields(
		map[string]interface{}{
			"env":     env,
			"service": service,
			"version": version,
		},
	).Logger()
}

func main() {
	lambda.Start(ddlambda.WrapHandler(HandleAPIGatewayProxyRequest, nil))
}

func HandleAPIGatewayProxyRequest(
	ctx context.Context,
	r *events.APIGatewayProxyRequest,
) (*events.APIGatewayProxyResponse, error) {

	span, ok := tracer.SpanFromContext(ctx)
	if !ok {
		log.Error().Msg("Unable to get current span from context")
	}
	span.SetOperationName("aws.apigw.request")
	span.SetTag(traceext.ResourceName, "aws.apigw.request")

	// Connect our traces and logs
	log.Logger = log.With().
		Uint64(tracing.LogDDTraceIDKey, span.Context().TraceID()).
		Uint64(tracing.LogDDSpanIDKey, span.Context().SpanID()).
		Logger()

	lctx, ok := lambdacontext.FromContext(ctx)
	if !ok {
		log.Error().Msg("Lambda context malformed or missing")
		return &events.APIGatewayProxyResponse{StatusCode: http.StatusInternalServerError}, nil
	}
	config, err := NewGtmCtrlConfig(ctx, lctx)
	if err != nil {
		log.Error().Err(err).Msg("Unable to create manager config")
		return &events.APIGatewayProxyResponse{StatusCode: http.StatusInternalServerError}, nil
	}
	eventBus := hpeventbus.NewSQSEventBus(
		&config.EventBusConfig,
		&config.SqsEventBusConfig,
		config.sqs,
		nil,
		config.SlackVerificationToken,
	)
	hpSvc := hpmanager.NewHousePointsService(&config.SlackConfig, &config.HousePointsConfig, eventBus)
	slackApp := slackapp.NewSlackApp(&config.SlackConfig)
	slackApp.RegisterService(hpSvc)
	proxy := NewApiGwProxy(config, slackApp)
	return proxy.HandleAPIGatewayRequest(r)
}
