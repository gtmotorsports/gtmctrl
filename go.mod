module gitlab.com/gtmotorsports/gtmctrl

go 1.16

require (
	github.com/DataDog/datadog-lambda-go v0.0.0
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/aws/aws-lambda-go v1.23.0
	github.com/aws/aws-sdk-go-v2 v1.3.3
	github.com/aws/aws-sdk-go-v2/config v1.1.6
	github.com/aws/aws-sdk-go-v2/service/kms v1.2.2
	github.com/aws/aws-sdk-go-v2/service/sqs v1.3.1
	github.com/rs/zerolog v1.21.0
	github.com/slack-go/slack v0.9.1 // indirect
	github.com/spf13/viper v1.7.1
	gitlab.com/johnrichter/house-points v0.1.0
	gitlab.com/johnrichter/house-points-manager v0.1.0
	gitlab.com/johnrichter/logging-go v0.1.0 // indirect
	gitlab.com/johnrichter/slack-app v0.1.0
	gitlab.com/johnrichter/tracing-go v0.1.0
	gopkg.in/DataDog/dd-trace-go.v1 v1.29.2
)

replace github.com/DataDog/datadog-lambda-go => github.com/johnrichter/datadog-lambda-go v0.10.1-0.20210411003902-0b23048cad43
